## How it works

Check out the [repo](https://github.com/MontFerret/ferret) for everything concerning the DSL.
- Clone the repo
- Get Open FaaS CLI
```bash
$ curl -sL https://cli.openfaas.com | sudo sh
```
-  Build and deploy
```bash
$ faas build -f openmag.yml \ 
 && faas deploy -f openmag.yml
```

- Execute the Query and invoke the Function
```bash
$ cat ./openmag.fql | faas invoke openmag > result.txt
```

## Capabilities 

We at Boring Works try to make every issue that we encounter as boring as possible #ABAP. Don't confuse it with SAP haha. However, This little Function is pretty powerful for our Analyzis Team, because it decouples the Scripting from the actual implementation and our Connect Team does not have to worry about Go Dep managment. 

(Boring) simplicity eat's complexity 💪🏾

A loving Thank you from Us to the Ferret contributor's and developers, who helped us along the way!!

## Next steps
We are currently working on a smaller Docker image `as of today almost 500 MB` 🙀.

Then we would implement it into our DevOps lifecycle.

Further steps is to automate the indexing of a Script to a Function and back to our Kotlin Backend.

Further thoughts in the Future is to create automated Functions from one proto file 
that include one Function at a time and is visible for Kotlin and other languages.